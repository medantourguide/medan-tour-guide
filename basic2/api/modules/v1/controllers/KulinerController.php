<?php
namespace app\api\modules\v1\controllers;
 
use yii\rest\ActiveController;
use Yii;
use yii\web\Response;
 
class KulinerController extends ActiveController
{
	
    // We are using the regular web app modules:
    public $modelClass = 'app\models\Kuliner';
}
