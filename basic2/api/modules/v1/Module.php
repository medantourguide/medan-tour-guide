<?php
// Check this namespace:
namespace app\api\modules\v1;
 
use yii;
use yii\web\Response;
 
class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();
        Yii::$app->response->format = Response::FORMAT_JSON;
        // ...  other initialization code ...
    }
}
