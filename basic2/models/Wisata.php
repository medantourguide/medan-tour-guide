<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "wisata".
 *
 * @property integer $id
 * @property string $nama
 * @property string $kategori
 * @property double $longitude
 * @property double $latitude
 * @property string $alamat
 * @property string $keterangan
 * @property string $gambar
 */
class Wisata extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wisata';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'kategori', 'longitude', 'latitude', 'alamat', 'keterangan', 'gambar'], 'required'],
            [['kategori'], 'string'],
            [['longitude', 'latitude'], 'number'],
            [['nama'], 'string', 'max' => 50],
            [['alamat'], 'string', 'max' => 30],
            [['keterangan'], 'string', 'max' => 100],
            [['gambar'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'kategori' => 'Kategori',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
            'alamat' => 'Alamat',
            'keterangan' => 'Keterangan',
            'gambar' => 'Gambar',
        ];
    }
}
