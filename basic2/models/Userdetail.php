<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $nama
 * @property string $email
 * @property string $password
 * @property string $love
 */
class Userdetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'email', 'password', 'love'], 'required'],
            [['love'], 'string'],
            [['nama'], 'string', 'max' => 20],
            [['email'], 'string', 'max' => 30],
            [['password'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'email' => 'Email',
            'password' => 'Password',
            'love' => 'Love',
        ];
    }
}
