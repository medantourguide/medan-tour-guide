package com.example.medantour.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.medantour.API.tourinterface;
import com.example.medantour.R;
import com.example.medantour.model.hotel;
import com.example.medantour.model.shopping;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by public on 12-Jun-16.
 */
public class ShoppingAdapter extends ArrayAdapter<shopping>{
    private Integer id;
    private String nama;
    private String kategori;
    private Double longitude;
    private Double latitude;
    private String alamat;
    private String keterangan;
    private String gambar;
    Context context;
   String url = "http://medan.esy.es/";

    private static LayoutInflater inflater = null;

    public ShoppingAdapter(Context context, int resource, List<shopping> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        View v = convertView;

        if(v == null){
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.shoplist,null);
        }
        shopping s = getItem(position);


            TextView tv_id = (TextView) v.findViewById(R.id.shop_id);
                tv_id.setText(s.getId().toString());

            TextView tv_nama = (TextView) v.findViewById(R.id.shop_nama);
                tv_nama.setText(s.getNama().toString());
            TextView tv_alamat = (TextView) v.findViewById(R.id.shop_alamt);
                tv_alamat.setText(s.getAlamat().toString());

        tv_id = (TextView) v.findViewById(R.id.shop_id);
        tv_id.setText(s.getId().toString());
        TextView tv_la = (TextView) v.findViewById(R.id.shop_latitude);
        tv_la.setText(s.getLatitude().toString());
        TextView tv_lo = (TextView) v.findViewById(R.id.shop_longitude);
        tv_lo.setText(s.getLongitude().toString());

            ImageView tv_img = (ImageView) v.findViewById(R.id.shop_image);
                Picasso.with(getContext()).load(url+s.getGambar()).into(tv_img);

        return v;
    }

}
