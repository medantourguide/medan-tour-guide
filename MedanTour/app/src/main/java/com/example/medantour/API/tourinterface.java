package com.example.medantour.API;
import android.text.Editable;

import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;

import com.example.medantour.model.hotel;
import com.example.medantour.model.kuliner;
import com.example.medantour.model.shopping;
import com.example.medantour.model.userdetail;
import com.example.medantour.model.wisata;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by priyanka on 22-May-16.
 */
public interface tourinterface {

    String url ="http://medan.esy.es/api/v1/";
    //registerPost
    @FormUrlEncoded
    @POST("/userdetails")
    void createLogin(
                @Field("nama") String username,
                @Field("email") String email,
                @Field("password") String password,
                Callback<userdetail> response);
    //userGet
    @GET("/userdetails")
    void getLogin(@Query("nama") String username, Callback<List<userdetail>> response);

    //WisataPost
    @FormUrlEncoded
    @POST("/wisatas")
    void createWisata(@Field("id") String id,
                      @Field("nama") String nama,
                      @Field("kategori") String kategori,
                      @Field("longitude") String longitude,
                      @Field("latitude") String latitude,
                      @Field("alamat") String alamat,
                      @Field("keterangan") String ket,
                      @Field("gambar") String image,
                      Callback<wisata> response);

    //WisataGet
    @GET("/wisatas")
    void getWisata(Callback<List<wisata>> response);

    //ShoppingPost
    @FormUrlEncoded
    @POST("/shoppings")
    void createShopping(@Field("id") String id,
                        @Field("nama") String nama,
                        @Field("kategori") String kategori,
                        @Field("longitude") String longitude,
                        @Field("latitude") String latitude,
                        @Field("alamat") String alamat,
                        @Field("keterangan") String ket,
                        @Field("gambar") String image,
                        Callback<shopping> response);
    //ShoppingGet
    @GET("/shoppings")
    void getShoppings(Callback<List<shopping>> response);


    //KulinerPost
    @FormUrlEncoded
    @POST("/kuliners")
    void createkuliner(@Field("id") String id,
                       @Field("nama") String nama,
                       @Field("kategori") String kategori,
                       @Field("longitude") String longitude,
                       @Field("latitude") String latitude,
                       @Field("alamat") String alamat,
                       @Field("keterangan") String ket,
                       @Field("gambar") String image,
                       Callback<kuliner> response);
    //KulinerGet
    @GET("/kuliners")
    void getKuliner(Callback<List<kuliner>> response );

    //HotelPost
    @FormUrlEncoded
    @POST("/hotels")
    void createhotel(@Field("id") String id,
                     @Field("nama") String nama,
                     @Field("kategori") String kategori,
                     @Field("longitude") String longitude,
                     @Field("latitude") String latitude,
                     @Field("alamat") String alamat,
                     @Field("keterangan") String ket,
                     @Field("gambar") String image,
                     Callback<hotel> response);
    //HotelGet
    @GET("/hotels")
    void getHotels(Callback<List<hotel>> response);


    //HotelDetail
       @GET("/hotels/{id}")
    void getHotelDetail(@Path("id") String id, Callback<hotel> response);

    @GET("/kuliners/{id}")
    void getKulinerDetail(@Path("id") String id, Callback<kuliner> response);

    @GET("/shoppings/{id}")
    void getShoppingDetail(@Path("id") String id, Callback<shopping> response);

    @GET("/wisatas/{id}")
    void getWisataDetail(@Path("id") String id, Callback<wisata> response);


}

