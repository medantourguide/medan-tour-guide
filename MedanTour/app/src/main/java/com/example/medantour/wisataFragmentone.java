package com.example.medantour;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.medantour.API.tourinterface;
import com.example.medantour.Adapter.HotelAdapter;
import com.example.medantour.Adapter.KulinerAdapter;
import com.example.medantour.Adapter.WisataAdapter;
import com.example.medantour.model.hotel;
import com.example.medantour.model.kuliner;
import com.example.medantour.model.wisata;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by priyanka on 6/7/16.
 */
public class wisataFragmentone extends Fragment {
    Integer id;

    public wisataFragmentone() {
        // Required empty public constructor
    }
    public wisataFragmentone(Integer s_id){
        id = s_id;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        LayoutInflater lf = getActivity().getLayoutInflater();
        final View v = lf.inflate(R.layout.wisata_fragment_one, null);

        /************
         * REST API *
         ************/
        //Building Retrofit Rest Adapter
        RestAdapter restAdapter = new RestAdapter.Builder()
                //set the path
                .setEndpoint(tourinterface.url)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        final tourinterface inf = restAdapter.create(tourinterface.class);

        inf.getWisataDetail(id.toString(), new Callback<wisata>() {
            @Override
            public void success(wisata item, Response response) {
                //pa = new PropertiAdapter(getBaseContext(), R.layout.list_properti, users);
                //detail.setDetail(item.getKeterangan().toString());
                ImageView image = (ImageView) v.findViewById(R.id.gambar_wisata);
                TextView nama = (TextView) v.findViewById(R.id.nama_wisata);
                TextView kategori = (TextView) v.findViewById(R.id.kategori_wisata);
                // TextView longitude = (TextView) v.findViewById(R.id.longitude_hotel);
                //TextView latitude = (TextView) v.findViewById(R.id.latitude_hotel);
                TextView alamat = (TextView) v.findViewById(R.id.alamat_wisata);
                TextView detail = (TextView) v.findViewById(R.id.detail_wisata);

                Picasso.with(getContext()).load(WisataAdapter.url+item.getGambar()).resize(200,200).into(image);
                nama.setText(item.getNama().toString());
                kategori.setText(item.getKategori().toString());
                //longitude.setText(item.getLongitude().toString());
                //latitude.setText(item.getLatitude().toString());
                alamat.setText(item.getAlamat().toString());
                detail.setText(item.getKeterangan().toString());
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity(), error.getResponse().getStatus() + " : " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        return v;

    }
}
