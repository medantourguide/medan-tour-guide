package com.example.medantour;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
/**
 * Created by public on 12-Jun-16.
 */
public class helper {

    public static void getListViewSize(ListView myListView)
    {
        ListAdapter myListAdapter = myListView.getAdapter();
        if(myListAdapter == null)
        {
            return;
        }
        int totalHeight = 0;
        for(int size=0; size<myListAdapter.getCount();size++)
        {
            View listItem = myListAdapter.getView(size,null,myListView);
            listItem.measure(0,0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = myListView.getLayoutParams();
        params.height = totalHeight + (myListView.getDividerHeight() * (myListAdapter.getCount()-1));
        myListView.setLayoutParams(params);
        Log.i("height of listitem:",String.valueOf(totalHeight));
    }
}
