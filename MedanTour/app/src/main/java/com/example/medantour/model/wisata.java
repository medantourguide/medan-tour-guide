package com.example.medantour.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by priyanka on 22-May-16.
 */
public class wisata {
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("nama")
        @Expose
        private String nama;
        @SerializedName("kategori")
        @Expose
        private String kategori;
        @SerializedName("longitude")
        @Expose
        private Double longitude;
        @SerializedName("latitude")
        @Expose
        private Double latitude;
        @SerializedName("alamat")
        @Expose
        private String alamat;
        @SerializedName("keterangan")
        @Expose
        private String keterangan;
        @SerializedName("gambar")
        @Expose
        private String gambar;

        /**
         *
         * @return
         * The id
         */
        public Integer getId() {
                return id;
        }

        /**
         *
         * @param id
         * The id
         */
        public void setId(Integer id) {
                this.id = id;
        }

        /**
         *
         * @return
         * The nama
         */
        public String getNama() {
                return nama;
        }

        /**
         *
         * @param nama
         * The nama
         */
        public void setNama(String nama) {
                this.nama = nama;
        }

        /**
         *
         * @return
         * The kategori
         */
        public String getKategori() {
                return kategori;
        }

        /**
         *
         * @param kategori
         * The kategori
         */
        public void setKategori(String kategori) {
                this.kategori = kategori;
        }

        /**
         *
         * @return
         * The longitude
         */
        public Double getLongitude() {
                return longitude;
        }

        /**
         *
         * @param longitude
         * The longitude
         */
        public void setLongitude(Double longitude) {
                this.longitude = longitude;
        }

        /**
         *
         * @return
         * The latitude
         */
        public Double getLatitude() {
                return latitude;
        }

        /**
         *
         * @param latitude
         * The latitude
         */
        public void setLatitude(Double latitude) {
                this.latitude = latitude;
        }

        /**
         *
         * @return
         * The alamat
         */
        public String getAlamat() {
                return alamat;
        }

        /**
         *
         * @param alamat
         * The alamat
         */
        public void setAlamat(String alamat) {
                this.alamat = alamat;
        }

        /**
         *
         * @return
         * The keterangan
         */
        public String getKeterangan() {
                return keterangan;
        }

        /**
         *
         * @param keterangan
         * The keterangan
         */
        public void setKeterangan(String keterangan) {
                this.keterangan = keterangan;
        }

        /**
         *
         * @return
         * The gambar
         */
        public String getGambar() {
                return gambar;
        }

        /**
         *
         * @param gambar
         * The gambar
         */
        public void setGambar(String gambar) {
                this.gambar = gambar;
        }
    }

