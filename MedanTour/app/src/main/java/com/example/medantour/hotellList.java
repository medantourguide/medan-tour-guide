package com.example.medantour;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.medantour.API.tourinterface;
import com.example.medantour.Adapter.HotelAdapter;
import com.example.medantour.model.hotel;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by public on 12-Jun-16.
 */
public class hotellList extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotelll);

        ImageView tol = (ImageView) findViewById(R.id.texthotel);
        tol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Main2Activity.class);
                startActivity(i);
            }
        });
        /************
         * REST API *
         ************/
        //Building Retrofit Rest Adapter
        RestAdapter restAdapter = new RestAdapter.Builder()
                //set the path
                .setEndpoint(tourinterface.url)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        final tourinterface inf = restAdapter.create(tourinterface.class);
        final ListView listView = (ListView) findViewById(R.id.hotellllist);
        inf.getHotels(new Callback<List<hotel>>() {

            @Override
            public void success(List<hotel> item, Response response2) {
                //pa = new PropertiAdapter(getBaseContext(), R.layout.list_properti, users);
                HotelAdapter ha = new HotelAdapter(getBaseContext(), R.layout.hotellist, item);
                listView.setAdapter(ha);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getBaseContext(), error.getResponse().getStatus() + " : " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               String s_id = ((TextView) view.findViewById(R.id.hotel_id)).getText().toString();
                String s_nama = ((TextView) view.findViewById(R.id.hotell_nama)).getText().toString();

                s_id = ((TextView) view.findViewById(R.id.hotel_id)).getText().toString();
                String s_longitude = ((TextView) view.findViewById(R.id.hotel_longitude)).getText().toString();
                String s_latitude = ((TextView) view.findViewById(R.id.hotel_latitude)).getText().toString();
                Intent i = new Intent(getApplicationContext(), HotelMain.class);
                i.putExtra("id", s_id);
                i.putExtra("latitude", s_latitude);
                i.putExtra("longitude", s_longitude);
                startActivity(i);

            }
        });

    }
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            Intent i = new Intent(getApplicationContext(),Main2Activity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_hotel) {
            Intent i = new Intent(getApplicationContext(),hotellList.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_shopping) {
            Intent i = new Intent(getApplicationContext(),shoplist.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_food) {
            Intent i = new Intent(getApplicationContext(),kulinerlist.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_place) {
            Intent i = new Intent(getApplicationContext(),wisatalist.class);
            startActivity(i);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
