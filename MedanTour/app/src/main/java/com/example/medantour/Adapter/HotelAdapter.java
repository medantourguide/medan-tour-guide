package com.example.medantour.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.medantour.R;
import com.example.medantour.model.hotel;
import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.util.List;

/**
 * Created by iin on 6/11/16.
 */
public class HotelAdapter extends ArrayAdapter<hotel> {
    private Integer id;
    private String nama;
    private String kategori;
    private Double longitude;
    private Double latitude;
    private String alamat;
    private String keterangan;
    private String gambar;
    public static String url = "http://medan.esy.es/";
    Context context;

    private static LayoutInflater inflater = null;

    public HotelAdapter(Context context, int resource, List<hotel> items) {
        super(context, resource, items);
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.hotellist, null);
        }

        hotel h = getItem(position);

        if (h != null) {
            TextView tv_id = (TextView) v.findViewById(R.id.hotel_id);
            TextView tv_nama = (TextView) v.findViewById(R.id.hotell_nama);
            TextView tv_la = (TextView) v.findViewById(R.id.hotel_latitude);
            TextView tv_lo = (TextView) v.findViewById(R.id.hotel_longitude);


            if (tv_nama != null) {
                tv_nama.setText(h.getNama().toString());
            }
          if (tv_id != null) {
                tv_id.setText(h.getId().toString());
            }
            if (tv_la != null) {
                tv_la.setText(h.getLatitude().toString());
            }
            if (tv_lo != null) {
                tv_lo.setText(h.getLongitude().toString());
            }

            TextView tv_kategori = (TextView) v.findViewById(R.id.hotell_alamat);
            if(tv_kategori !=null)
            {
                tv_kategori.setText(h.getAlamat().toString());
            }
           ImageView tv_img =  (ImageView) v.findViewById(R.id.hotell_image);
            if(tv_img!=null)
            {
                Picasso.with(getContext()).load(url+h.getGambar()).into(tv_img);
            }
        }


        return v;
    }

}
