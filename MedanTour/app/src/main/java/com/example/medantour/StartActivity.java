package com.example.medantour;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

public class StartActivity extends AppCompatActivity {

    //Spalash screend timer
    private static int SPLASH_TIME_OUT = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        final ObjectAnimator animation = ObjectAnimator.ofInt(progressBar,"progress",5000);
        Thread splashThread = new Thread(){
            @Override
        public void run(){
                try{
                    int waited = 10;
                    while(waited<=50){
                        sleep(100);
                        progressBar.setProgress(waited * 2);
                        waited ++;
                    };
                }catch(InterruptedException e){

                }finally {
                    startActivity(new Intent(getBaseContext(),Main2Activity.class));
                    finish();
                    //stiop();
                }
            }
        };
        splashThread.start();


    }

}
