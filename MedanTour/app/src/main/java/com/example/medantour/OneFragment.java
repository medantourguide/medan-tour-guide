package com.example.medantour;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.medantour.API.tourinterface;
import com.example.medantour.Adapter.HotelAdapter;
import com.example.medantour.model.hotel;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by iin on 6/7/16.
 */
public class OneFragment extends Fragment {
    String id;

    public OneFragment() {
        // Required empty public constructor
    }
    public OneFragment(String s_id){
        id = s_id;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        LayoutInflater lf = getActivity().getLayoutInflater();
        final View v = lf.inflate(R.layout.fragment_one, null);
        /************
         * REST API *
         ************/
        //Building Retrofit Rest Adapter
        RestAdapter restAdapter = new RestAdapter.Builder()
                //set the path
                .setEndpoint(tourinterface.url)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        final tourinterface inf = restAdapter.create(tourinterface.class);


        inf.getHotelDetail(id, new Callback<hotel>() {

            @Override
            public void success(hotel item, Response response2) {
                String data_gambar, data_alamat, data_latitude, data_longitude, data_kategori, data_nama, data_keterangan;
                data_gambar = item.getGambar();
                data_alamat = item.getAlamat();
                data_latitude = item.getLatitude().toString();
                data_longitude = item.getLongitude().toString();
                data_kategori = item.getKategori();
                data_nama = item.getNama();
                data_keterangan = item.getKeterangan();

                ImageView image = (ImageView) v.findViewById(R.id.gambar_hotel);
                TextView nama = (TextView) v.findViewById(R.id.nama_hotel);
                TextView kategori = (TextView) v.findViewById(R.id.kategori_hotel);
               TextView longitude = (TextView) v.findViewById(R.id.hotel_longitude);
                TextView latitude = (TextView) v.findViewById(R.id.hotel_latitude);
                TextView alamat = (TextView) v.findViewById(R.id.alamat_hotel);
                TextView detail = (TextView) v.findViewById(R.id.detail_hotel);


                Picasso.with(getContext()).load(HotelAdapter.url+item.getGambar()).resize(200,200).into(image);
                nama.setText(item.getNama().toString());
                kategori.setText(item.getKategori().toString());
                //longitude.setText(item.getLongitude().toString());
                //latitude.setText(item.getLatitude().toString());
                alamat.setText(item.getAlamat().toString());
                detail.setText(item.getKeterangan().toString());
                Picasso.with(getContext()).load(tourinterface.url + data_gambar).resize(200, 200).into(image);
                nama.setText(data_nama);
                kategori.setText(data_kategori);
                longitude.setText(data_longitude);
                latitude.setText(data_latitude);
                alamat.setText(data_alamat);
                detail.setText(data_keterangan);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity(), error.getResponse().getStatus() + " : " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


        return v;

    }
}
