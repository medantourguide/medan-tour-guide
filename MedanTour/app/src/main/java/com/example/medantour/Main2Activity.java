package com.example.medantour;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Animation fade_out, fade_in;
        ViewFlipper viewFlipper;

        viewFlipper = (ViewFlipper) this.findViewById(R.id.back);
        fade_out = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
        fade_in = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
        viewFlipper.setInAnimation(fade_in);
        viewFlipper.setOutAnimation(fade_out);

        ImageView img1 = (ImageView) findViewById(R.id.img1);
        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        //sets ayyto flipper
        viewFlipper.setAutoStart(true);
        viewFlipper.setFlipInterval(5000);
        viewFlipper.startFlipping();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView hotels = (TextView)findViewById(R.id.hotels);
        hotels.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), hotellList.class);
                startActivity(i);
            }
        });

        TextView shop = (TextView)findViewById(R.id.shop);
        shop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), shoplist.class);
                startActivity(i);
            }
        });
        TextView food = (TextView)findViewById(R.id.food);
        food.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), kulinerlist.class);
                startActivity(i);
            }
        });
        TextView place = (TextView)findViewById(R.id.place);
        place.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), wisatalist.class);
                startActivity(i);
            }
        });
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        /*if (id == R.id.nav_account) {
            // Handle the camera action
        } else */
        if (id == R.id.nav_home) {
            Intent i = new Intent(getApplicationContext(),Main2Activity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_hotel) {
            Intent i = new Intent(getApplicationContext(),hotellList.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_shopping) {
            Intent i = new Intent(getApplicationContext(),shoplist.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_food) {
            Intent i = new Intent(getApplicationContext(),kulinerlist.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_place) {
            Intent i = new Intent(getApplicationContext(),wisatalist.class);
            startActivity(i);
            finish();
        } /*else if (id == R.id.nav_about){

        }else if (id == R.id.nav_login){
        Intent i = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(i);
        finish();
        } else if (id == R.id.nav_register){
        Intent a = new Intent(getApplicationContext(),registerActivity.class);
        startActivity(a);
        finish();
        }*/
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
        }
        }
