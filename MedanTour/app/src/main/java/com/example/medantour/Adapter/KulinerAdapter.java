package com.example.medantour.Adapter;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.medantour.R;
import com.example.medantour.model.kuliner;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by priyanka on 12-Jun-16.
 */
public class KulinerAdapter extends ArrayAdapter<kuliner>{
    private Integer id;
    private String nama;
    private String kategori;
    private Double longitude;
    private Double latitude;
    private String alamat;
    private String keterangan;
    private String gambar;
    Context context;
   public static String url = "http://medan.esy.es/";

    private static LayoutInflater inflater = null;
    public KulinerAdapter(Context context, int resource, List<kuliner> items)
    {
        super(context,resource,items);
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        View v = convertView;
        if(v== null)
        {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.kuliner_list,null);
        }
        kuliner k = getItem(position);

        if(k!=null)
        {
<<<<<<< HEAD
            TextView tv_id = (TextView) v.findViewById(R.id.kuliner_id);
            if(tv_id!=null)
            {
                tv_id.setText(k.getId().toString());
            }
            TextView tv_nama = (TextView) v.findViewById(R.id.kuliner_nama);
            if(tv_nama !=null)
            {
=======
            TextView tv_nama = (TextView) v.findViewById(R.id.kuliner_nama);
            if(tv_nama !=null)
            {
>>>>>>> 6c5bfc682ab313cc6a9823c4c231f0be24ee79a3
                tv_nama.setText(k.getNama().toString());
            }
            TextView tv_alamat = (TextView) v.findViewById(R.id.kuliner_alamat);
            if(tv_alamat !=null)
            {
                tv_alamat.setText(k.getAlamat().toString());
            }
            ImageView tv_img = (ImageView) v.findViewById(R.id.kuliner_image);
            if(tv_img !=null){
                Picasso.with(getContext()).load(url+k.getGambar()).into(tv_img);
            }
            TextView tv_id = (TextView) v.findViewById(R.id.kuliner_id);
            tv_id.setText(k.getId().toString());
            TextView tv_la = (TextView) v.findViewById(R.id.kuliner_latitude);
            tv_la.setText(k.getLatitude().toString());
            TextView tv_lo = (TextView) v.findViewById(R.id.kuliner_longitude);
            tv_lo.setText(k.getLongitude().toString());

        }
        return v;

    }
}

