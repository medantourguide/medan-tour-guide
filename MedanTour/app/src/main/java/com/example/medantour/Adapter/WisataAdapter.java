package com.example.medantour.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.medantour.R;
import com.example.medantour.model.wisata;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by public on 12-Jun-16.
 */
public class WisataAdapter extends ArrayAdapter<wisata>{

    private Integer id;
    private String nama;
    private String kategori;
    private Double longitude;
    private Double latitude;
    private String alamat;
    private String keterangan;
    private String gambar;
    Context context;
   public static String url = "http://medan.esy.es/";

    private static LayoutInflater inflater = null;
    public WisataAdapter(Context context, int resource, List<wisata> items){
        super(context,resource,items);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.wisata_list, null);
        }
        wisata w = getItem(position);

        if (w != null) {
<<<<<<< HEAD

            TextView tv_id = (TextView) v.findViewById(R.id.wisata_id);
            if(tv_id!=null)
            {
                tv_id.setText(w.getId().toString());
            }
=======
>>>>>>> 6c5bfc682ab313cc6a9823c4c231f0be24ee79a3
            TextView tv_nama = (TextView) v.findViewById(R.id.wisata_nama);
            if (tv_nama != null) {
                tv_nama.setText(w.getNama().toString());
            }
            TextView tv_alamat = (TextView) v.findViewById(R.id.wisata_alamat);
            if (tv_alamat != null) {
                tv_alamat.setText(w.getAlamat().toString());
            }
            TextView tv_id = (TextView) v.findViewById(R.id.wisata_id);
            tv_id.setText(w.getId().toString());
            TextView tv_la = (TextView) v.findViewById(R.id.wisata_latitude);
            tv_la.setText(w.getLatitude().toString());
            TextView tv_lo = (TextView) v.findViewById(R.id.wisata_longitude);
            tv_lo.setText(w.getLongitude().toString());



            ImageView tv_img = (ImageView) v.findViewById(R.id.wisata_image);
            if (tv_img != null) {
                Picasso.with(getContext()).load(url + w.getGambar()).into(tv_img);
            }
        }
        return v;
    }
}
