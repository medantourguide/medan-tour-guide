package com.example.medantour;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.medantour.API.tourinterface;
import com.example.medantour.model.userdetail;
import java.util.List;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.Callback;
public class MainActivity extends AppCompatActivity {

    EditText username, pass;
    Button b_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b_login = (Button) findViewById(R.id.btnlogin);
        username = (EditText) findViewById(R.id.username);
        pass = (EditText) findViewById(R.id.password);

        b_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RestAdapter adapter = new RestAdapter.Builder().setEndpoint(tourinterface.url).build();
                final tourinterface tourinterface = adapter.create(tourinterface.class);
                tourinterface.getLogin(username.getText().toString(), new Callback<List<userdetail>>() {
                    @Override
                    public void success(List<userdetail> userdetail, Response response) {
                        boolean success = false;
                        for(int j=0; j<userdetail.size();j++){
                        if(username.getText().toString().equals(userdetail.get(j).getNama())&& pass.getText().toString().equals(userdetail.get(j).getPassword()))
                        {
                            Intent i;
                            i = new Intent(getApplicationContext(),Main2Activity.class);
                            i.putExtra("username",userdetail.get(j).getNama());
                            i.putExtra("id", userdetail.get(j).getId());
                            startActivity(i);
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),"Username or password is wrong", Toast.LENGTH_SHORT).show();
                        }
                    }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if(error.getMessage().equals("404 Not Found")){
                            Toast.makeText(getApplicationContext(),"Username doesn't exist",Toast.LENGTH_SHORT).show();
                        }

                    }
                });


        }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

   @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
