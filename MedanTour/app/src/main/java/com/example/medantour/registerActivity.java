package com.example.medantour;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.medantour.API.tourinterface;
import com.example.medantour.model.userdetail;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class registerActivity extends AppCompatActivity {

    Button b2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
       // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);


        b2 = (Button) findViewById(R.id.btnregis);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText nama = (EditText) findViewById(R.id.username_regis);
                EditText email = (EditText) findViewById(R.id.email_regis);
                EditText password = (EditText) findViewById(R.id.password_regis);

                RestAdapter adapter = new RestAdapter.Builder().setEndpoint(tourinterface.url).build();
                final tourinterface tourinterface = adapter.create(tourinterface.class);

                tourinterface.createLogin(
                        nama.getText().toString(),
                        email.getText().toString(),
                        password.getText().toString(),
                        new Callback<userdetail>() {
                            @Override
                            public void success(userdetail userdetail, Response response) {
                                Toast.makeText(getBaseContext(), "Done", Toast.LENGTH_LONG).show();
                                Intent i;
                                i = new Intent(getApplicationContext(), Main2Activity.class);
                                startActivity(i);
                                finish();
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                if(error.getMessage().equals("404 Not Found")){
                                    Toast.makeText(getApplicationContext(),"Username doesn't exist",Toast.LENGTH_SHORT).show();
                                }

                            }
                        }
                );

            }
        });
    }
}
