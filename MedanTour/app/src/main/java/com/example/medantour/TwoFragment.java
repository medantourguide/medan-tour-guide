package com.example.medantour;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.medantour.API.tourinterface;
import com.example.medantour.model.hotel;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by iin on 6/7/16.
 */
public class TwoFragment extends Fragment  {
    private Integer id;
    private SupportMapFragment fragment;
    private GoogleMap map;

    private String latitude, longitude;

    public TwoFragment() {
        // Required empty public constructor
    }
    public TwoFragment(String latitude, String longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_two, container,
                false);
        /*latitude = Float.parseFloat(this.getArguments().getString("latitude"));
        longitude = Float.parseFloat(this.getArguments().getString("longitude"));*/

        FragmentManager fm = getChildFragmentManager();
        fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
        if (fragment == null) {
            fragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map, fragment).commit();
        }

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (map == null) {
            map = fragment.getMap();
            CameraUpdate camera = CameraUpdateFactory.newLatLng(
                    new LatLng(
                            Double.parseDouble(latitude),
                            Double.parseDouble(longitude)
                    ));
            map.addMarker(new MarkerOptions().position(
                    new LatLng(
                            Double.parseDouble(latitude),
                            Double.parseDouble(longitude)
                    )));
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
            map.moveCamera(camera);
            map.animateCamera(zoom);
        }
    }
}
