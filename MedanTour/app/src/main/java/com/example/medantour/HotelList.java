package com.example.medantour;

import com.example.medantour.API.tourinterface;
import com.example.medantour.Adapter.HotelAdapter;
import com.example.medantour.model.hotel;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.os.Bundle;
import android.app.Activity;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

public class HotelList extends Activity {
    static final LatLng TutorialsPoint = new LatLng(21 , 57);
    private GoogleMap googleMap;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_list);

        /*******************************
         *          Google Map         *
         ******************************
        try {
            if (googleMap == null) {
         googleMap = ((MapFragment) getFragmentManager().
                        findFragmentById(R.id.map)).getMap();
            }
            googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
            Marker TP = googleMap.addMarker(new MarkerOptions().
                    position(TutorialsPoint).title("TutorialsPoint"));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
*/
        /********************************
         *          Floating Button     *
         ********************************/
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Swipe (geser) ke kanan untuk melihat list hotel", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        /***********
         * Drawer  *
         ***********/
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
            /************
             * REST API *
             ************/
            //Building Retrofit Rest Adapter
            RestAdapter restAdapter = new RestAdapter.Builder()
                //set the path
                .setEndpoint(tourinterface.url)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
            final tourinterface inf = restAdapter.create(tourinterface.class);
            final ListView listView = (ListView) findViewById(R.id.left_drawer);
            inf.getHotels(new Callback<List<hotel>>() {

                @Override
                public void success(List<hotel> item, Response response2) {
                    //pa = new PropertiAdapter(getBaseContext(), R.layout.list_properti, users);
                    HotelAdapter ha = new HotelAdapter(getBaseContext(), R.layout.hotel_list, item);
                    listView.setAdapter(ha);
                }


                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getBaseContext(),error.getResponse().getStatus()+" : "+error.getMessage(), Toast.LENGTH_LONG).show();
                }
            });

    }
}