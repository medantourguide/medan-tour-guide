package com.example.medantour;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.medantour.API.tourinterface;
import com.example.medantour.model.hotel;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;



/**
 * Created by iin on 6/7/16.
 */
public class HotelMain extends FragmentActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private String id;

    /*private String data_gambar;
    private String data_nama;*/
    private String data_latitude;
    private String data_longitude;
    /*private String data_kategori;
    private String data_keterangan;
    private String data_alamat;*/


    static final LatLng TutorialsPoint = new LatLng(21 , 57);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toolbar_main);

        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        data_latitude = intent.getStringExtra("latitude");
        data_longitude = intent.getStringExtra("longitude");/*
        Toast.makeText(getBaseContext(), data_latitude, Toast.LENGTH_LONG).show();
        Toast.makeText(getBaseContext(), data_longitude, Toast.LENGTH_LONG).show();*/
/*
        toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);*/


        ImageView img = (ImageView) findViewById(R.id.gbrhotel);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),hotellList.class);
                startActivity(i);
            }
        });
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());


        /************
         * REST API *
         ************/
        //Building Retrofit Rest Adapter
       /* RestAdapter restAdapter = new RestAdapter.Builder()
                //set the path
                .setEndpoint(tourinterface.url)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        final tourinterface inf = restAdapter.create(tourinterface.class);
        inf.getHotelDetail(id, new Callback<hotel>() {

            @Override
            public void success(hotel item, Response response2) {
                data_latitude = item.getLatitude().toString();
                data_longitude = item.getLongitude().toString();

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getBaseContext(), error.getResponse().getStatus() + " : " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        Toast.makeText(getBaseContext(), data_latitude, Toast.LENGTH_LONG).show();
        Toast.makeText(getBaseContext(), data_longitude, Toast.LENGTH_LONG).show();*/
        TwoFragment tf = new TwoFragment(data_latitude, data_longitude);
        OneFragment of = new OneFragment(id);
        adapter.addFragment(of, "Detail");
        adapter.addFragment(tf, "Location");
        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
