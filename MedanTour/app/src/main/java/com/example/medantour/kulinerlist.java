package com.example.medantour;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
<<<<<<< HEAD
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
=======
import android.view.View;
import android.widget.AdapterView;
>>>>>>> 6c5bfc682ab313cc6a9823c4c231f0be24ee79a3
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.medantour.API.tourinterface;
import com.example.medantour.Adapter.KulinerAdapter;
import com.example.medantour.Adapter.ShoppingAdapter;
import com.example.medantour.model.kuliner;
import com.example.medantour.model.shopping;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by public on 12-Jun-16.
 */
public class kulinerlist extends Activity implements NavigationView.OnNavigationItemSelectedListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kuliner);

        ImageView tol = (ImageView) findViewById(R.id.textfood);
        tol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Main2Activity.class);
                startActivity(i);
            }
        });
        /************
         * REST API *
         ************/
        //Building Retrofit Rest Adapter
        RestAdapter restAdapter = new RestAdapter.Builder()
                //set the path
                .setEndpoint(tourinterface.url)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        final tourinterface inf = restAdapter.create(tourinterface.class);
        final ListView listView = (ListView) findViewById(R.id.kulinerlist);
        inf.getKuliner(new Callback<List<kuliner>>() {
            @Override
            public void success(List<kuliner> item, Response response) {
                KulinerAdapter kul = new KulinerAdapter(getBaseContext(),R.layout.kuliner_list,item);
                listView.setAdapter(kul);
            }
            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getBaseContext(), error.getResponse().getStatus() + " : " + error.getMessage(), Toast.LENGTH_LONG).show();
            }

        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String s_id = ((TextView) view.findViewById(R.id.kuliner_id)).getText().toString();
                String s_longitude = ((TextView) view.findViewById(R.id.kuliner_longitude)).getText().toString();
                String s_latitude = ((TextView) view.findViewById(R.id.kuliner_latitude)).getText().toString();
                Intent i = new Intent(getApplicationContext(), HotelMain.class);
                i.putExtra("id", s_id);
                i.putExtra("latitude", s_latitude);
                i.putExtra("longitude", s_longitude);
                startActivity(i);

<<<<<<< HEAD
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String s_id = ((TextView) view.findViewById(R.id.kuliner_id)).getText().toString();
                String s_nama = ((TextView) view.findViewById(R.id.kuliner_nama)).getText().toString();
                Intent a = new Intent(getApplicationContext(),KulinerMain.class);
                a.putExtra("id",s_id);
                a.putExtra("nama",s_nama);
                startActivity(a);
=======
>>>>>>> 6c5bfc682ab313cc6a9823c4c231f0be24ee79a3
            }
        });
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            Intent i = new Intent(getApplicationContext(),Main2Activity.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_hotel) {
            Intent i = new Intent(getApplicationContext(),hotellList.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_shopping) {
            Intent i = new Intent(getApplicationContext(),shoplist.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_food) {
            Intent i = new Intent(getApplicationContext(),kulinerlist.class);
            startActivity(i);
            finish();
        } else if (id == R.id.nav_place) {
            Intent i = new Intent(getApplicationContext(),wisatalist.class);
            startActivity(i);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}



